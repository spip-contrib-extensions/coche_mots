<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cocher_groupe' => 'Cocher les mots-clés du groupe',

	// P
	'pour_objet' => 'pour l\'objet',
	
);

